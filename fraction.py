#coding=utf-8
'''
2. Дробь
Даны два числа: a и b. Найдите значение числа a/b, записанного в k-ичной системе счисления. Если a/b — периодическая дробь, то период следует заключить в скобки.

Пример входных данных:
1 2 8
1 12 10

Пример выходных данных:
0.4
0.08(3)
'''

import sys,math,tobase

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def run():
    if sys.argv[1:]:
        # открываем файл с данными
        try:
            input_data = open(sys.argv[1],'r')
        except:
            print bcolors.FAIL + 'Error opening file ' + sys.argv[1] + '. The program is stopped.' + bcolors.ENDC
            exit()
        else:
            print bcolors.WARNING + 'The data will be loaded from a file - ' + sys.argv[1] + '.' + bcolors.ENDC
            data = input_data.readlines()
            input_data.close()
    else:
        # вводим вручную
        data = []
        input_data = ''
        print bcolors.WARNING + 'Enter the numerator, the denominator and the radix.' + bcolors.ENDC
        print bcolors.WARNING + 'To complete the entry, use the «end».' + bcolors.ENDC

        while True:
            input_data = raw_input()
            if input_data == 'end': break
            data.append(input_data)

    output_data = []
    x = ''
    y = ''
    k = ''
    counter = 0
    flag = 0
    # сделать проверку на 0
    for i in data: 
        for j in i: # получаем числитель
            counter+=1
            if not j.isdigit():
                if flag == 1:
                    break # уходим получив числитель
                continue
            else:
                x+=j
                flag = 1
        flag = 0
        for j in i[ counter - 1 :]: # получаем знаменатель
            counter+=1
            if not j.isdigit():
                if flag == 1:
                    break # уходим получив знаменатель
                continue
            else:
                y+=j
                flag = 1
        flag = 0
        for j in i[ counter - 1 :]: # получаем основание
            if not j.isdigit():
                if flag == 1:
                    break # уходим получив основание
                continue
            else:
                k+=j
                flag = 1

        # проверка
        print 'x = ', x, '   y = ', y , '   k = ', k
        if int(x) == 0 or int(y) == 0 or int(k) == 0:
            print bcolors.FAIL + 'Set x:%s, y:%s, k:%s' % (x,y,k) + ' - value x or y or k is 0. This set will be skipped.' + bcolors.ENDC
        else:
            output_data.append((x,y,k))
        # обнуляем значения
        x = ''
        y = ''
        k = ''
        flag = 0
        counter = 0

    # находим значение
    for i in output_data:
        x = int(i[0])
        y = int(i[1])
        base = int(i[2])
        print bcolors.OKGREEN + '------------------------------------------------' + bcolors.ENDC
        print bcolors.OKGREEN + 'Base : ' + i[2] + bcolors.ENDC
        print bcolors.OKGREEN + 'Value %s/%s the base 10 is %f' % (x,y, float(x)/float(y)) + bcolors.ENDC        
        print bcolors.OKGREEN + 'Value %s/%s the base %s is %s' % (x,y,base, tobase.main( x ,y ,base )) + bcolors.ENDC

if __name__ == '__main__':
    run()