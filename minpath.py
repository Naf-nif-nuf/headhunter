#coding=utf-8
'''1. Минимальное расстояние
Дан набор из N точек на плоскости (для простоты можно считать, что у всех точек целочисленные координаты). 
Найдите минимальное расстояние между двумя точками из этого набора.
Пример входных данных:
10 10
20 10
20 15
Пример выходных данных:
5
'''

import sys,math

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def run():
    if sys.argv[1:]:
        # открываем файл с данными
        try:
            input_data = open(sys.argv[1],'r')
        except:
            print bcolors.FAIL + 'Error opening file ' + sys.argv[1] + '. The program is stopped.' + bcolors.ENDC
            exit()
        else:
            print bcolors.WARNING + 'The coordinates of the points will be loaded from a file - ' + sys.argv[1] + '.' + bcolors.ENDC
            points = input_data.readlines()
            input_data.close()
    else:
        # вводим вручную
        points = []
        input_data = ''
        print bcolors.WARNING + 'Enter the coordinates of the points.' + bcolors.ENDC
        print bcolors.WARNING + 'To complete the entry, use the «end».' + bcolors.ENDC

        while True:
            input_data = raw_input()
            if input_data == 'end': break
            points.append(input_data)

    output_data = []
    x = ''
    y = ''
    counter = 0
    flag = 0
    for i in points: 
        for j in i: # получаем первую координату точки
            counter+=1
            if not j.isdigit():
                if flag == 1:
                    break # уходим получив первую координату
                continue
            else:
                x+=j
                flag = 1
        flag = 0
        for j in i[ counter - 1 :]: # получаем вторую координату точки
            if not j.isdigit():
                if flag == 1:
                    break # уходим получив вторую координату
                continue
            else:
                y+=j
                flag = 1
        # проверка
        # print 'x = %s, y = %s' % (x, y)
        output_data.append((x,y))
        # обнуляем значения
        x = ''
        y = ''
        flag = 0
        counter = 0

    response = []
    flag = 0
    for i in output_data:
        for j in output_data:
            if i != j:
                dist = math.sqrt( pow( int(j[0])- int(i[0]),2) +  pow( int(j[1])- int(i[1]),2) ) 
                # проверка
                # print 'i[0]= %s, i[1]= %s, j[0]= %s, j[1]= %s , dist = %d' % (i[0],i[1],j[0],j[1],dist)
                response.append(dist)

    print bcolors.OKGREEN + 'The minimum distance between two points is equal to this set: ' + '%.4f' % min(response) + bcolors.ENDC

if __name__=='__main__':
    run()
